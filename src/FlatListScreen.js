import React from "react";
import { Text, StyleSheet, View, Button, FlatList } from "react-native";

const FlatListScreen = () => {

  const friends = [
    { name: 'friend 1' },
    { name: "friend 2" },
    { name: "friend 3" },
    { name: "friend 4" },
    { name: "friend 5" },
    { name: "friend 6" },
  ];
  return (

    <FlatList
      keyExtractor={friend => { friend.name }}
      data={friends}
      renderItem={({ item }) => {
        return <Text style={styles.textstyle}>{item.name}</Text>;

      }}>


    </FlatList>
  );
};

const styles = StyleSheet.create({

  textstyle: {
    marginVertical: 50
  }
}
);



export default FlatListScreen;