import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";
import { exp } from "react-native/Libraries/Animated/src/Easing";

const BoxModel = () => {

  return (

    <View>

      <View style={styles.viewOneStyle}>
        <Text style={styles.textStyle}>App</Text>

      </View>

      <View>
        <View style={styles.viewmiddleStyle}>

          <View style={styles.viewTwoStyle}></View>
          <View style={styles.viewThreeStyle}></View>

        </View>

        <View style={styles.viewFourStyle}></View>

      </View>


    </View>
  );

};

const styles = StyleSheet.create({

  textStyle:
  {
    alignSelf: "center",
    marginVertical: 20,
    fontSize: 25

  },

  viewStyle: {

    borderColor: 'black',
    borderWidth: 1,

  },

  viewmiddleStyle: {

    flexDirection: "row",
    justifyContent: "space-between"

  },


  viewOneStyle: {

    borderColor: 'black',
    borderWidth: 1,

  },

  viewTwoStyle: {

    borderColor: 'orange',
    borderWidth: 1,
    height: 80,
    width: 100,
    backgroundColor: 'orange',

  },

  viewThreeStyle: {

    borderColor: 'purple',
    borderWidth: 1,
    height: 80,
    width: 100,
    backgroundColor: 'purple',

  },

  viewFourStyle: {

    borderColor: 'green',
    borderWidth: 1,
    height: 80,
    width: 100,
    backgroundColor: 'green',
    alignSelf: "center"
  },


});

export default BoxModel;