import React, { useReducer, useState } from "react";
import { Text, StyleSheet, View, Button } from "react-native";
import ColorCounter from './components/ColorCounter';

const SquareScreenReducer = () => {

  const reducer = (state, action) => {

    //state==={red:number,green:number,blue:number}
    //action==={colortochange:'red'|| 'green'||'blue',amount:15|-15}



    switch (action.ColorToChange) {
      case 'red':
        return state.red + action.amount > 255 || state.red + action.amount < 0
          ? state
          : { ...state, red: state.red + action.amount };
      case 'green':
        return state.green + action.amount > 255 || state.green + action.amount < 0
          ? state
          : { ...state, green: state.green + action.amount };

      case 'blue':
        return state.blue + action.amount > 255 || state.blue + action.amount < 0
          ? state
          : { ...state, blue: state.blue + action.amount };

      default:
        return state;

    }

  };

  //inside dispatch action will be called
  const [state, dispatch] = useReducer(reducer, { red: 0, green: 0, blue: 0 });
  const { red, green, blue } = state;
  const COLOR_INCREMENT = 15;

  return (

    <View>
      <Text style={{ marginBottom: 20, fontWeight: "400" }}>Square Screen</Text>
      <ColorCounter color="red"
        onIncrease={() => { dispatch({ ColorToChange: 'red', amount: COLOR_INCREMENT }) }}
        onDecrease={() => { dispatch({ ColorToChange: 'red', amount: -1 * COLOR_INCREMENT }) }} />
      <ColorCounter color="green"
        onIncrease={() => { dispatch({ ColorToChange: 'green', amount: COLOR_INCREMENT }) }}
        onDecrease={() => { dispatch({ ColorToChange: 'green', amount: -1 * COLOR_INCREMENT }) }} />
      <ColorCounter color="blue"
        onIncrease={() => { dispatch({ ColorToChange: 'blue', amount: COLOR_INCREMENT }) }}
        onDecrease={() => { dispatch({ ColorToChange: 'blue', amount: -1 * COLOR_INCREMENT }) }} />

      <View style={{ height: 150, width: 150, backgroundColor: `rgb(${red},${green},${blue})` }}></View>
    </View>

  );
};

export default SquareScreenReducer;