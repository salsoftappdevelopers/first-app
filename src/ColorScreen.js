import React, { useState } from "react";
import { Text, StyleSheet, View, Button, FlatList } from "react-native";

const ColorScreen = () => {

  const [colors, setColors] = useState([]);

  return (

    <View>

      <Button title="Add a color"
        onPress={() => {
          setColors([...colors, RandomRGB()]);
        }}>

      </Button>
      <FlatList
        keyExtractor={item => item}
        data={colors}
        renderItem={({ item }) => {
          return <View style={{ height: 50, width: 50, backgroundColor: item, borderRadius: 10, alignSelf: "center", marginTop: 20 }}>

          </View>;
        }}>

      </FlatList>
    </View>


  );
};

const RandomRGB = () => {

  const red = Math.floor(Math.random() * 256);
  const green = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);

  return `rgb(${red},${green},${blue})`;
};

const styles = StyleSheet.create({});

export default ColorScreen