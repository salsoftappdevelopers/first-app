import React, { useState } from "react";
import { Text, StyleSheet, View, Button, FlatList } from "react-native";
import { TextInput } from "react-native-gesture-handler";

const TextScreen = () => {

  const [name, setName] = useState('');
  
  return (

    <View>

      <Text style={{ marginHorizontal: 20, fontSize: 20, fontWeight: "bold", marginTop: 20 }}>Enter Password:</Text>

      <TextInput style={styles.input}
        autoCorrect={false}
        autoCapitalize="none"
        value={name}
        onChangeText={(newValue) => { setName(newValue) }}
      />

      <Text style={{ alignSelf: "center", marginTop: 20 }}>
        My name is {name}
      </Text>

      {name.length < 5 ? <Text>Password must be longer than 5 characters</Text> : null}
    </View>



  );

};

const styles = StyleSheet.create({

  input: {
    marginHorizontal: 20,
    marginTop: 20,
    borderWidth: 1,
    borderColor: 'black',
  }

});

export default TextScreen;