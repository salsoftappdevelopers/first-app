import React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";

const componentscreen = () => {

  const greeting = <Text> hello </Text>;
  const name = 'hiba';
  const name_title = <Text>My name is {name}</Text>;

  return (
    <View>
      <Text style={styles.textStyle}>Get started with react native </Text>
      <Text style={styles.Subheadertitle}>{name_title}</Text>

    </View>
  );
};

const styles = StyleSheet.create(
  {
    textStyle: {
      fontSize: 45

    },

    Subheadertitle: {
      fontSize: 20

    }
  }
);
export default componentscreen