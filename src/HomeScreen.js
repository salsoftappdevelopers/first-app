import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";

const HomeScreen = ({ navigation }) => {
  return (

    <View>
      <Text style={styles.text}>Home</Text>
      <Button title='Go to Image screen'
        onPress={() => navigation.navigate('Image')}
      />
      <TouchableOpacity onPress={() => console.log('list button pressed')} >

        <Text>Go to list screen</Text>
      </TouchableOpacity>

    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    marginLeft: 50

  }



});

export default HomeScreen;

