import React, { useReducer, useState } from "react";
import { Text, StyleSheet, View, Button } from "react-native";

const CounterScreenReducer = () => {


  const reducer = (state, action) => {
    //state==={counter:number}
    //action==={Type:increment || decrement,amountToChange:1|-1}

    switch (action.WhatisChanging) {
      case 'Increment':
        return state.counter + action.amountToChange < 0
          ? state
          : { ...state, counter: state.counter + action.amountToChange };

      case 'Decrement':
        return state.counter - action.amountToChange < 0
          ? state
          : { ...state, counter: state.counter - action.amountToChange };

      default:
        return state;

    }

  };


  const [state, dispatch] = useReducer(reducer, { counter: 0 });
  const { counter } = state;


  return (

    <View>
      <Button title="Increase"
        onPress={() => { dispatch({ WhatisChanging: 'Increment', amountToChange: 1 }) }} />
      <Button title="Decrease"
        onPress={() => { dispatch({ WhatisChanging: 'Decrement', amountToChange: 1 }) }} />

      <Text>Current Count: {counter}</Text>
    </View>

  );
};

export default CounterScreenReducer