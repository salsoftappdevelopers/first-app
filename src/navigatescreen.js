import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";
import { exp } from "react-native/Libraries/Animated/src/Easing";

const navigatescreen = ({ navigation }) => {

  return (

    <View >

      <View style={style.textstyle}>
        <Button

          onPress={() => { navigation.navigate("Image") }}
          title="Go to Image Screen">

        </Button>

      </View>

      <View style={style.textstyle}>

        <Button

          onPress={() => { navigation.navigate("Flat List") }}
          title="Go to Flat List Screen"></Button>


      </View>

      <View style={style.textstyle}>

        <Button

          onPress={() => { navigation.navigate("CounterReducer") }}
          title="Go to Counter Screen"></Button>
      </View>

      <View style={style.textstyle}>

        <Button

          onPress={() => { navigation.navigate("Color") }}
          title="Go to Color Screen"></Button>
      </View>

      <View style={style.textstyle}>

        <Button

          onPress={() => { navigation.navigate("Reducer") }}
          title="Go to Square Screen"></Button>
      </View>

      <View style={style.textstyle}>

        <Button

          onPress={() => { navigation.navigate("Text Screen") }}
          title="Go to Text Screen"></Button>
      </View>

      <View style={style.textstyle}>

        <Button

          onPress={() => { navigation.navigate("Box Screen") }}
          title="Go to Box Model Screen"></Button>
      </View>



    </View>



  );

};

const style = StyleSheet.create({
  textstyle: {
    marginTop: 50
  }
}
);

export default navigatescreen;