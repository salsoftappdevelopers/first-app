import React from "react";
import { Text, StyleSheet, View, FlatList } from "react-native";

const ListSscren = () => {

  const friends = [

    { name: 'Friend #1', age: 'Age 20' },
    { name: 'Friend #2', age: 'Age 15' },
    { name: 'Friend #3', age: 'Age 10' },
    { name: 'Friend #4', age: 'Age 40' },
    { name: 'Friend #5', age: 'Age 30' },
    { name: 'Friend #6', age: 'Age 12' },
    { name: 'Friend #7', age: 'Age 30' },

  ]

  return (

    <FlatList

      keyExtractor={(friend) => friend.name}
      data={friends}
      renderItem={({ item }) => {

        return <Text style={styles.textstyle}>{item.name} - {item.age}</Text>;
      }}>
    </FlatList>
  );

};

const styles = StyleSheet.create(

  {
    textstyle:
    {

      marginVertical: 50
    }

  }
)

export default ListSscren