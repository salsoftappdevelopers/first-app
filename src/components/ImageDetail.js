import React from "react";
import { Text, StyleSheet, View, Image } from "react-native";

const ImageDetail = props => {

  return (

    <View>

      <Image source={props.image} />
      <Text>{props.title}</Text>
      <Text>Image Score - {props.score}</Text>

    </View>

  );

};
export default ImageDetail