import React from "react";
import { Text, StyleSheet, View } from "react-native";
import ImageDetail from '../src/components/ImageDetail';

const ImageScreen = () => {

  return (

    <View>

      <ImageDetail title="Forest"
        image={require('../../first-app/assets/forest.jpg')}
        score="10" />
      <ImageDetail title="Beach"
        image={require('../../first-app/assets/beach.jpg')}
        score="5" />
      <ImageDetail title="Mountain"
        image={require("../../first-app/assets/mountain.jpg")}
        score="4" />

    </View>
  )
};

export default ImageScreen