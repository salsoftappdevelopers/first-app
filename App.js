/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar, ColorPropType
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from './src/HomeScreen';
import ImageScreen from './src/ImageScreen';
import navigatescreen from './src/navigatescreen';
import CounterScreen from './src/CounterScreen';
import FlatListScreen from './src/FlatListScreen';
import ColorScreen from './src/ColorScreen';
import SquareScreen from './src/SquareScreen';
import SquareScreenReducer from './src/SquareScreenReducer'
import CounterScreenReducer from './src/CounterScreenReducer';
import TextScreen from './src/TextScreen';
import BoxModel from './src/BoxModel';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="navigate" >
        <Stack.Screen name="navigate" component={navigatescreen}></Stack.Screen>
        <Stack.Screen name="counter" component={CounterScreen}></Stack.Screen>
        <Stack.Screen name="Home" component={HomeScreen}></Stack.Screen>
        <Stack.Screen name="Image" component={ImageScreen}></Stack.Screen>
        <Stack.Screen name="Color" component={ColorScreen}></Stack.Screen>
        <Stack.Screen name="Flat List" component={FlatListScreen}></Stack.Screen>
        <Stack.Screen name="Square" component={SquareScreen}></Stack.Screen>
        <Stack.Screen name="Reducer" component={SquareScreenReducer}></Stack.Screen>
        <Stack.Screen name="CounterReducer" component={CounterScreenReducer}></Stack.Screen>
        <Stack.Screen name="Text Screen" component={TextScreen}></Stack.Screen>
        <Stack.Screen name="Box Screen" component={BoxModel}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({

});

export default App;
