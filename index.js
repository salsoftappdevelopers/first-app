/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import HomeScreen from './src/HomeScreen';
import componentscreen from './src/componentscreen';
import ListSscren from './src/ListSscreen';
import ImageScreen from './src/ImageScreen';
import CounterScreen from './src/CounterScreen';
import ColorScreen from './src/ColorScreen';
import SquareScreen from './src/SquareScreen';
import FlatListScreen from './src/FlatListScreen';
import 'react-native-gesture-handler';



AppRegistry.registerComponent(appName, () => App);


